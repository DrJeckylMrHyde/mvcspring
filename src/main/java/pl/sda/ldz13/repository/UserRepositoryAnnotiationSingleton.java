package pl.sda.ldz13.repository;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.exeptions.UserNotFoundExeption;
import pl.sda.ldz13.model.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class UserRepositoryAnnotiationSingleton implements UserRepository {
    private Logger log = Logger.getLogger(UserRepositoryAnnotiationSingleton.class);
    private List<User> list;

    public User addUser(String name, int age) {
        long id = System.nanoTime();
        User user = new User(id, name, age);
        list.add(user);
        return user;
    }

    public User getUserById(long id) {
        return list.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new UserNotFoundExeption("User not found"));
    }

    @Override
    public User modifyUser(User user) {
        User userFound = getUserById(user.getId());
        userFound.setAge(user.getAge());
        userFound.setName(user.getName());
        return userFound;
    }

    @Override
    public boolean deleteUser(Long id) {
        return list.removeIf(user -> user.getId().longValue() == id);
    }

    public List<User> getUserByName(String name) {
        return list.stream()
                .filter(user -> user.getName().toLowerCase().contains(name.toLowerCase()))
                .collect(Collectors.toList());
    }

    public List<User> getAllUsers() {
        return list;
    }

    @PostConstruct
    public void init() {
        list = new ArrayList<User>();
        log.info("Initilized Users List (Annotation)");
    }

    @PreDestroy
    public void destroy() {
        log.info("Object was destory (Annotation)");
    }
}
