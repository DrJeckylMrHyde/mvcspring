package pl.sda.ldz13.repository;

import pl.sda.ldz13.model.User;

import java.util.List;

public interface UserRepository {
    User addUser(String name, int age);

    User getUserById(long id);

    User modifyUser(User user);

    boolean deleteUser(Long id);

    List<User> getUserByName(String name);

    List<User> getAllUsers();

    void init();

    void destroy();
}
