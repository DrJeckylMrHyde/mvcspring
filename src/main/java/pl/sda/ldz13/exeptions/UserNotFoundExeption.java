package pl.sda.ldz13.exeptions;

public class UserNotFoundExeption extends RuntimeException {
    public UserNotFoundExeption(String message){
        super(message);
    }
}
