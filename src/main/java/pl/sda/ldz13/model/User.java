package pl.sda.ldz13.model;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;

public class User {
    private Long id;
    @NotBlank(message = "Nazwa nie może być bez wartości")
    private String name;
    @Min(value = 0, message = "Wiek nie może być ujemny")
    private int age;

    public User(Long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public User(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
