package pl.sda.ldz13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.model.User;
import pl.sda.ldz13.repository.UserRepository;

import java.util.List;

@Service
public class UserServiceSingleton implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User addUser(User user) {
        return userRepository.addUser(user.getName(), user.getAge());
    }

    @Override
    public User getUserById(Long id) {

        return userRepository.getUserById(id);
    }

    @Override
    public User modifyUser(User user) {
        return userRepository.modifyUser(user);
    }

    @Override
    public boolean deleteUser(Long id) {
        return userRepository.deleteUser(id);
    }

    @Override
    public List<User> getUsersByName(String name) {
        return userRepository.getUserByName(name);
    }


    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
}
